import os
from PIL import Image

sprites = [img for img in[file for file in os.listdir('.')] if img.endswith(".png")]

for sprite_path in sprites:
    sprite = Image.open(sprite_path)

    idxs = [x * sprite.height for x in range(0, int(sprite.width / sprite.height))]
    for i in idxs:
        region = (i, 0, i + sprite.height, sprite.height)
        frame = sprite.crop(region)
        frame = frame.transpose(Image.FLIP_LEFT_RIGHT)
        sprite.paste(frame, region)

    out_path = sprite_path[:-4] + "_rev" + ".png"
    sprite.save(out_path, "PNG")
