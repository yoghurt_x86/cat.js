class vec2 {
    x = 0.0
    y = 0.0

    constructor(x = 0.0, y = 0.0){
        this.x = x;
        this.y = y;
    }

    set(x, y) {
        this.x = x;
        this.y = y;
        return this;
    }

    inc(vec){
        this.x = this.x + vec.x;
        this.y = this.y + vec.y;
        return this;
    }

    scale(scalar){
        this.x = this.x * scalar;
        this.y = this.y * scalar;
        return this;
    }

    length(){
        return Math.sqrt((this.x * this.x) + (this.y * this.y));
    }

    normalize(){ // Loses precison. Don't use repeatedly
        var l = this.length();
        if(l > 0.0){
            this.x = this.x / l;
            this.y = this.y / l;
        }
        return this;
    }

    distTo(vec){
        var dx, dy;
        dx = vec.x - this.x;
        dy = vec.y - this.y;
        return Math.sqrt((dx * dx) + (dy * dy));
    }

    copy(){ // call to make some garbage
        return new vec2(this.x ,this.y);
    }
}

class cat {
    static max_speed = 1.8;
    static images = {
        standing : './sprites/cat_standing.png',
        standing_rev : './sprites/cat_standing_rev.png',
        walking: './sprites/cat_walking.png',
        walking_rev : './sprites/cat_walking_rev.png',
        sitting  : './sprites/cat_sitting.png',
        sitting_rev  : './sprites/cat_sitting_rev.png',
        sleeping : './sprites/cat_sleeping.png',
        sleeping_rev : './sprites/cat_sleeping_rev.png'
    }

    pos = new vec2(); // Position
    vel = new vec2(); // Velocity
    mov = new vec2(); // Move intention

    input = {
        u : new vec2(), // Inputs: up left down right
        l : new vec2(), 
        d : new vec2(), 
        r : new vec2()
    }

    loop; // Current animation sprite-sheet

    // Animation state variables:
    tick = -1;
    time_lastmovement;
    time_lastsit;
    sitting = false;

    target = null; // Target to walk toward

    constructor(x,y) {
        this.pos = new vec2(x, y);
        this.time_lastmovement = performance.now();
        this.time_lastsit = performance.now();
    }
    update(t) { // game loop update function
        this.ticks++;
        this.mov.set(0.0,0.0);

        if(this.target != null){
            this.mov.x = this.target.x - this.pos.x; 
            this.mov.y = this.target.y - this.pos.y; 
            this.mov.normalize();
            if(this.pos.distTo(this.target) < 1.0){
               this.target = null; // goal reached... remove target!
            }
        }

        this.mov.inc(this.input.u);
        this.mov.inc(this.input.l);
        this.mov.inc(this.input.d);
        this.mov.inc(this.input.r);
        this.mov.normalize();

        this.vel.x = ((this.vel.x * 5) + (this.mov.x * cat.max_speed)) / 6.0; 
        this.vel.y = ((this.vel.y * 5) + (this.mov.y * cat.max_speed)) / 6.0; 

        if(this.vel.length() > 0.1){
            this.time_lastmovement = t;
            this.resting = false;
        }

        this.pos.inc(this.vel);

        var rev = this.vel.x >= 0; // Reverse animation?

        if(this.vel.length() > 0.15){
            this.loop = rev ?
                        cat.images.walking
                      : cat.images.walking_rev;
        } else {
            this.loop = rev ?
                        cat.images.standing
                      : cat.images.standing_rev;
        }
        // If the cat wants to rest
        if((t - this.time_lastmovement) > 3000){
            this.loop = rev ? 
                        cat.images.sitting
                      : cat.images.sitting_rev;
            if(!this.resting){
                this.time_lastsit = t;
                this.resting = true;
            // If cat has rested long enough to fall asleep.
            } 
            else if ((t - this.time_lastsit) > 12000){ 
                this.loop = rev ?
                            cat.images.sleeping
                          : cat.images.sleeping_rev;
            }
        }
    } 

    // draw the cat;
    render(ctx,t ) { 
        // update frame
        this.tick = this.tick + 1;
        var frame = Math.floor(this.tick / 16); // change frame every 16 tick

        if(this.loop.width / this.loop.height <= frame){
            this.tick = 0;
            frame = 0;
        }

        ctx.drawImage(this.loop, frame * this.loop.height, 0, this.loop.height, this.loop.height, this.pos.x - 48 , this.pos.y - 48, 96, 96);
    }  
   
    keypress = (e) => {
        if(e.repeat) return;
        switch(e.key){
            case 'w': this.input.u.set( 0.0, -1.0); break;
            case 'a': this.input.l.set(-1.0,  0.0); break;
            case 's': this.input.d.set( 0.0,  1.0); break;
            case 'd': this.input.r.set( 1.0,  0.0); break;
        }
    }
    keyrelease = (e) => {
        switch(e.key){
            case 'w': this.input.u.set(0.0,0.0); break;
            case 'a': this.input.l.set(0.0,0.0); break;
            case 's': this.input.d.set(0.0,0.0); break;
            case 'd': this.input.r.set(0.0,0.0); break;
        }
    }

    select(){
        document.addEventListener('keydown', this.keypress);
        document.addEventListener('keyup', this.keyrelease);
    }
    unselect(){
        document.removeEventListener('keydown', this.keypress);
        document.removeEventListener('keyup', this.keyrelease);
    }

}

class cat_wander_behavior {
    cat; // The cat

    bx1; // Bounds for the cat to stay in.
    bx2;
    by1;
    by2;

    last_action;

    constructor(cat, min, max){
        this.bx1 = min.x;
        this.bx2 = max.x;
        this.by1 = min.y;
        this.by2 = max.y;

        this.cat = cat;
        this.last_action = performance.now();
    }   

    setbounds(bx1, by1, bx2, by2) {
        this.bx1 = bx1;
        this.by1 = by1;
        this.bx2 = bx2;
        this.by2 = by2;
    }
    
    // get a point that's inside the bounds and close to the cat
    get_position_inside(){ 
        var tx, ty;
        if(this.cat.pos.x < this.bx1){
            tx = this.bx1 + 72;
        } else if(this.cat.pos.x > this.bx2){
            tx = this.bx2 - 72;
        } else {
            tx = this.cat.pos.x;
        }
        if(this.cat.pos.y < this.by1){
            ty = this.by1 + 72;
        } else if(this.cat.pos.y > this.by2){
            ty = this.by2 - 72;
        } else {
            ty = this.cat.pos.y;
        }
        return new vec2(tx, ty);
    }

    update(t){
        this.setbounds(0.0, window.pageYOffset, window.innerWidth, window.innerHeight + window.pageYOffset);
        
        // If there's input from the user, cancel target :
        if(this.cat.input.u.y != 0.0 
        || this.cat.input.d.y != 0.0
        || this.cat.input.r.x != 0.0
        || this.cat.input.l.x != 0.0){
            this.cat.target = null;
        }

        // If the cat is doing something, update the time for last action :
        if(this.cat.target != null  
        || this.cat.mov.x != 0.0 
        || this.cat.mov.y != 0.0){
            this.last_action = t;
            return;
        }
        
        // If the cat is outside bounds. bring it back! :
        if(this.cat.target == null  
        &&(this.cat.pos.x < this.bx1 
        || this.cat.pos.x > this.bx2 
        || this.cat.pos.y < this.by1
        || this.cat.pos.y > this.by2)
        ){
            this.cat.target = this.get_position_inside();
        }

        // If cat wants to wander somewhere else :
        if(this.cat.target == null 
        && t - this.last_action > 24000 + (120000.0 * Math.random())
        && Math.random() < 0.01){
            this.last_action = t;
            this.cat.target = new vec2(this.bx1 + (Math.random() * (this.bx2 - this.bx1)) , this.by1 + (Math.random() * (this.by2 - this.by1)));
        }
    }
}

// GLOBAL STATE VARIABLES //
var canvas;        // The canvas 
var ctx;           // canvas context
var cats = [];     // Must be cats
var behavior = []; // each object here must have an update function that takes the current time.

var draw = (t) => {
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0,0, canvas.width, canvas.height)
    ctx.translate(0, -window.pageYOffset);

    behavior.forEach(b => {
        b.update(t);
    })
    cats.forEach(c => {
        c.update(t);
        c.render(ctx, t);
    }); 
    window.requestAnimationFrame(draw);
}


function init() {
    // preload images
    for (const [key, value] of Object.entries(cat.images)) { 
        var img = new Image();
        img.src = value;
        Object.defineProperty(cat.images, key, {value: img});
    }

    canvas = document.createElement('canvas');
    canvas.setAttribute('height', window.innerHeight);
    canvas.setAttribute('width' , window.innerWidth );

    canvas.style.position = 'fixed';
    canvas.style.left = 0;
    canvas.style.top = 0;
    canvas.style.zIndex = 10;
    canvas.style.pointerEvents = 'none';
    window.addEventListener('resize', () => {
        canvas.setAttribute('height', window.innerHeight);
        canvas.setAttribute('width' , window.innerWidth );
    });

    document.body.insertAdjacentElement('afterbegin', canvas)
    document.body.addEventListener('onscroll', (e) => {
        document.body.scrollTop;
    });

    ctx = canvas.getContext('2d');
    ctx.webkitImageSmoothingEnabled = false;
    ctx.msImageSmoothingEnabled = false;
    ctx.imageSmoothingEnabled = false;

    window.requestAnimationFrame(draw);
}

function init_cat(){
    init();
    var actor = new cat(100, 125);
    actor.select();
    cats.push(actor);
    
    var wander = new cat_wander_behavior(actor, new vec2(0.0, 0.0), new vec2(canvas.width, canvas.height));
    // Remember to tell the wander behavior when we scroll
    window.addEventListener('resize', (e) => {
        wander.setbounds(0.0, window.pageYOffset, window.innerWidth, window.innerHeight + window.pageYOffset);
    });
    behavior.push(wander);
    
    // quick behavior object to make the cat scroll the page! 
    var scroll = {
        window_cat: new vec2(0.0,0.0),

        // Transforms page coordinate to window coordinate. 
        // Only based on vertical scroll 
        // Output is saved in the 'to' vector passed to the function
        window_pos_of: function (vec, to) { 
            return to.set(vec.x, vec.y - window.pageYOffset);
        },
        update: function(t) {
            this.window_pos_of(actor.pos, this.window_cat);
            var p = this.window_cat.y * 100 / window.innerHeight;
            if      ( p < 8 && actor.input.u.y < 0.0 ) {
                window.scrollBy(0.0, actor.vel.y * 1.14);
            }
            else if ( p > 92 && actor.input.d.y > 0.0){
                window.scrollBy(0.0, actor.vel.y * 1.14);
            }
        }
    };
    behavior.push(scroll);
}

export {vec2, cat, cat_wander_behavior, init, init_cat, cats, behavior, canvas, ctx}
